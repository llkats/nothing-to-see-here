'use strict'

import React from 'react'
import ReactDOM from 'react-dom'

import DoveContainer from './components/DoveContainer'

ReactDOM.render(<DoveContainer />, document.querySelector('main'))
