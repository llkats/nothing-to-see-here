'use strict'

import React from 'react'

class AddDoveForm extends React.Component {

  constructor(props) {
    super(props)

    this.onSubmit = this.onSubmit.bind(this)
  }

  onSubmit(e) {
    console.log(this.refs)
  }

  render() {
    return (
      <form className="dove-add" onSubmit={this.onSubmit}>
        <fieldset>
          <h2>Add A Dove</h2>
          <label>
            <div className="add-label">Id:</div>  <input ref={(input) => { this.idInput = input }} type="text" name="id" id="id" />
          </label>

          <label>
            <div className="add-label">Currently Active?</div> <select ref={(input) => { this.activeSelect = input }} name="is-active" id="is-active">
              <option value="not-active">not active</option>
              <option value="active">active</option>
            </select>
          </label>

          <label>
            <div className="add-label">Color:</div> <input ref={(input) => { this.colorInput = input }} type="color" name="color" id="color" />
          </label>

          <label>
            <div className="add-label">Images Collected:</div> <input ref={(input) => { this.imagesInput = input }} type="number" name="images-collected" id="images-collected" />
          </label>

          <label>
            <div className="add-label">Last Command:</div> <input ref={(input) => { this.commandInput = input }} type="text" name="last-command" id="last-command" />
          </label>

          <button type="submit">add!</button>
        </fieldset>
      </form>
    )
  }
}

export default AddDoveForm
