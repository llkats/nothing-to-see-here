'use strict'

import AppView from './AppView'
import {Container} from 'flux/utils'
import DoveActions from './DoveActions'
import DoveStore from './DoveStore'

function getStores() {
  return [
    DoveStore
  ]
}

function getState() {
  return {
    doves: DoveStore.getState(),

    addDove: DoveActions.addDove,
    onLoad: DoveActions.getDoves,
  }
}

export default Container.createFunctional(AppView, getStores, getState)
