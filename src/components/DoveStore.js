'use strict'

const Immutable = require('immutable')
const ReduceStore = require('flux/utils').ReduceStore
const Dove = require('./Dove')
const DoveActionTypes = require('./DoveActionTypes')
const DoveDispatcher = require('./DoveDispatcher')

class DoveStore extends ReduceStore {
  constructor() {
    super(DoveDispatcher)
  }

  getInitialState() {
    return Immutable.OrderedMap()
  }

  reduce(state, action) {
    switch (action.type) {
      case DoveActionTypes.GET_DOVES:
        return action.doves

      case DoveActionTypes.RECEIVED_DOVES:
        return state.concat(action.doves)

      case DoveActionTypes.ADD_DOVE:

        return state

      default:
        return state
    }
  }
}

module.exports = new DoveStore()
