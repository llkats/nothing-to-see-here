'use strict'

const DoveActionTypes = require('./DoveActionTypes')
const DoveDispatcher = require('./DoveDispatcher')

const URL = 'http://localhost:3000/doves'

const Actions = {
  getDoves() {
    fetch(URL)
      .then((response) => {
        if (response.ok) {
          return response.json()
            .then(function (res) {
              DoveDispatcher.dispatch({
                type: DoveActionTypes.GET_DOVES,
                doves: res
              })
            })
        }
        console.log('error getting doves from the server')
      })
  },

  receivedDoves(doves) {
    DoveDispatcher.dispatch({
      type: DoveActionTypes.GET_DOVES,
      doves: doves
    })
  },

  addDove(dove) {
    DoveDispatcher.dispatch({
      type: DoveActionTypes.ADD_DOVE,
      dove: dove
    })
  }
}

export default Actions
