'use strict'

const Immutable = require('immutable')

const Dove = Immutable.Record({
  id : '',
  active : false,
  color : '',
  images_collected: 0,
  last_command: '',
  deorbit_dt: ''
})

module.exports = Dove
