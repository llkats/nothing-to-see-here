'use strict'

const ActionTypes = {
  GET_DOVES: 'GET_DOVES',
  RECEIVED_DOVES: 'RECEIVED_DOVES',
  ADD_DOVE: 'ADD_DOVE'
}

export default ActionTypes
