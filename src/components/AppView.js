'use strict'

import React from 'react'
import { render } from 'react-dom'
import ReactTable from 'react-table'

import AddDoveForm from './AddDoveForm'
import DoveDispatcher from './DoveDispatcher'

import 'react-table/react-table.css'
import '../styles/index.css'

const columns = [{
  header:'Doves',
  columns: [{
    header: 'ID',
    accessor: 'id',
    maxWidth: '50'
  }, {
    header: 'Active',
    accessor: 'active',
    render: props => <span className="dove-active">{ props.row.active ? 'active' : 'not active' }</span>
  }, {
    header: 'Color',
    accessor: 'color'
  }, {
    header: 'Images Collected',
    accessor: 'images_collected'
  }, {
    header: 'Last Command',
    accessor: 'last_command'
  }, {
    header: 'Deorbit Date',
    accessor: 'deorbit_dt'
  }]
}]


function Main(props) {
  if (!props.doves.length) {
    props.onLoad()
  }

  return (
    <div className="container">
      <h1>Dove Mission Control</h1>
      <ReactTable
        data={props.doves}
        columns={columns}
        style={
          {width: '800px'}
        }
      />
      <AddDoveForm {...props} />
    </div>
  )
}

export default Main;

