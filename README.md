# Dove Mission Control

This was as far as I was able to get with the technical assessment.

## Technical Details
The project uses Flux, Immutable, and [react-table](https://react-table.js.org) for data display.

To start the servers, please run `npm start`. Data is still served from `http://localhost:3000`, while the project itself can be viewed at `http://localhost:8080/`.

## Features
The project successfully displays a table populated with data from the JSON API, and includes a form for adding a new dove. However, the form does not successfully add a dove to the data, and the search feature and additional feature remain unimplemented.
